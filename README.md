# Sharkio

A hobby project for managing my [Shark's](https://en.wikipedia.org/wiki/Red-tailed_black_shark) tank, using Arduinos and a Raspberry Pi.

MVP is a internet accessible thermometer reading and history.