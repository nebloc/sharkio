package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"time"

	"github.com/tarm/serial"
)

type Command rune

const TempReq Command = 't'
const PhReq Command = 'p'

func main() {
	c := &serial.Config{Name: "/dev/ttyACM0", Baud: 9600}
	rw, err := serial.OpenPort(c)
	if err != nil {
		fmt.Println(err)
	}

	w := bufio.NewWriter(rw)

	for x := 0; x < 10; x++ { // Loop forever normally
		w.WriteByte(byte(TempReq))
		w.Flush()
		fmt.Printf("Requested temp reading: %c\n", TempReq)

		temp, err := readTemp(rw)
		if err != nil {
			fmt.Printf("error reading temp: %v\n", err)
		} else {
			fmt.Printf("Got Temp: %d°C\n", temp)
			switch {
			case temp < 25:
				fmt.Println("Too cold")
				//alert()
			case temp > 28:
				fmt.Println("Too hot")
				//alert()
			case temp > 27:
				fmt.Println("Bit hot")
			case temp < 26:
				fmt.Println("Bit cold")
			default:
				fmt.Println("Fish are OK")
			}
		}
		time.Sleep(20 * time.Second)
	}
}

type Message struct {
	Value   uint8  `json:"value"`
	Ok      bool   `json:"ok"`
	Message string `json:"message"`
}

func readTemp(reader io.Reader) (uint8, error) {
	scanner := bufio.NewScanner(reader) // bufio gives us the ability to read and write single bytes easily
	errChan := make(chan error, 1)      // errChan is for handling errors reading from serial
	valChan := make(chan string, 1)     // valChan is for handling the temp value

	// Start goroutine to listen to serial
	go func() {
		defer close(valChan) // let sender close channel
		defer close(errChan) // let sender close channel

		for scanner.Scan() {
			valChan <- scanner.Text()
			return
		}
		if err := scanner.Err(); err != nil {
			errChan <- err
			return
		}
	}()

	timeout := time.NewTicker(3 * time.Second) // If serial doesn't respond in time
	defer timeout.Stop()

	select {
	case b := <-valChan:
		msg := &Message{}
		fmt.Println(b)
		err := json.Unmarshal([]byte(b), msg)
		if err != nil {
			return 0, err
		}
		if !msg.Ok {
			return 0, fmt.Errorf("message from arduino: ", msg.Message)
		}
		return msg.Value, nil
	case err := <-errChan:
		return 0, fmt.Errorf("couldn't read from buffer: ", err)
	case <-timeout.C:
		return 0, fmt.Errorf("timeout reached")
	}
}
