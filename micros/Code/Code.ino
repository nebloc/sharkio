/**
  Auther: Ben Coleman
  Date: 08/01/2020

  Arduino is set up to wait for a byte to be sent via Serial.
  The byte is read as a char and matched to a command. The Command
  is then run and the results are written back on the serial port.
  The return data has a control byte to indicate the message type, then
  the payload.
  For string message types (i.e. error) the value should be read until 
  a new line.
  For numeric messages (i.e. temp reading) the value should be read as a
  single byte.
  
  For example:
  01100101  01110101 01101110 01101011 01101110 01101111 01110111 01101110 00100000 01100011 01101111 01101101 01101101 01100001 01101110 01100100
  e         unknown command\r\n
  
  01110100  00011010
  t         26
*/
void setup() {
  Serial.begin(9600); // Setup serial lib and set baud rate 9600
  while (!Serial) {} // Wait for serial to be ready
}

// TODO: Move to enum
const char tempCommand = 't';
const char phCommand = 'p';
char command = ' ';

void loop() {
  Serial.readBytes(&command, 1); // Read command
  switch (command) {
    case tempCommand:
      tempReq();
      break;
    case phCommand:
      phReq();
      break;
    default: // UNKNOWN COMMAND
      Serial.write('e');
      Serial.println("Unknown Command");
      break;
  }
}

// tempReq reads and returns the temperature in Celsius
void tempReq() {
  // TODO: read temperature
  int temp = 1;

  Serial.write(tempCommand);
  Serial.write(temp);
}

// phReq reads and returns the PH balance (1-14)
void phReq() {
  // TODO: read PH balance
  int ph = 1;

  Serial.write(phCommand);
  Serial.write(ph);
}
