package protocol

import (
	"bufio"
	"bytes"
	"strings"
	"testing"
)

func Test_readMessage_err(t *testing.T) {
	tcs := []struct {
		Input    string
		Expected string
	}{
		{
			Input:    "eunknown control code\r\n",
			Expected: "unknown control code",
		},
	}

	for i, tc := range tcs {
		t.Logf("Testcase %d", i)
		r := bufio.NewReader(strings.NewReader(tc.Input))
		msg, err := readMessage(r)
		if err == nil {
			t.Log("Expected an error", msg)
			t.Fail()
		}
		if err.Error() != tc.Expected {
			t.Logf("\r\nExpected:\r\n%s\r\nGot:\r\n%s", tc.Expected, err.Error())
			t.Fail()
		}
	}
}

func Test_readMessage_ints(t *testing.T) {
	tcs := []struct {
		Input []byte
		Value uint8
		Type  Code
	}{
		{
			Input: []byte{'t', 16},
			Value: 16,
			Type:  TempCode,
		},
		{
			Input: []byte{'p', 16},
			Value: 16,
			Type:  PhCode,
		},
	}

	for i, tc := range tcs {
		t.Logf("Test case %d", i)
		r := bufio.NewReader(bytes.NewReader(tc.Input))
		msg, err := readMessage(r)
		if err != nil {
			t.Errorf("Unexpected error:", err)
		}
		if tc.Value != msg.Value {
			t.Errorf("\r\nExpected value:\r\n%d\r\nGot:\r\n%d", tc.Value, msg.Value)
		}
		if tc.Type != msg.Type {
			t.Errorf("\r\nExpected control code:\r\n%s\r\nGot:\r\n%s", tc.Type, msg.Type)
		}
	}
}

func Test_readMessage_multiple(t *testing.T) {
	tcs := []Message{
		{
			Type:  TempCode,
			Value: 26,
		},
		{
			Type:  PhCode,
			Value: 7,
		},
		{
			Type:  TempCode,
			Value: 27,
		},
	}
	r := bufio.NewReader(bytes.NewReader([]byte{'t', 26, 'p', 7, 't', 27}))
	for x, tc := range tcs {
		msg, err := readMessage(r)
		if err != nil {
			t.Errorf("Error on loop %d: %v", x, err)
			return
		}
		if tc.Type != msg.Type {
			t.Errorf("Expected %s got %s", tc.Type, msg.Type)
		}
		if tc.Value != msg.Value {
			t.Errorf("Expected %d got %d", tc.Value, msg.Value)
		}
	}
}
