package protocol

import (
	"bufio"
	"bytes"
	"testing"
)

func Test_writeCommand(t *testing.T) {
	tcs := []Code{TempCode, PhCode}
	for i, tc := range tcs {
		t.Logf("Test case %d", i)
		var buf bytes.Buffer
		err := writeCommand(bufio.NewWriter(&buf), tc)
		if err != nil {
			t.Errorf("Unexpected error:", err)
		}
		if buf.Len() > 1 {
			t.Errorf("Expected length of buffer to be 1, got %d", buf.Len())
		}
		b := Code(buf.Bytes()[0])
		if b != tc {
			t.Errorf("Expected %b | %s, got %b | %s", tc, tc, b, b)
		}
	}
}
