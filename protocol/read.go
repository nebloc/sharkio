package protocol

import (
	"bufio"
	"fmt"
	"github.com/pkg/errors"
	"io"
	"strings"
)

func (c *Connection) ReadMessage() (Message, error) {
	return readMessage(c.reader)
}

func readMessage(r *bufio.Reader) (Message, error) {
	ctl, err := r.ReadByte()
	if err != nil {
		return Message{}, err
	}
	switch Code(ctl) {
	case TempCode, PhCode: // UINT8 control types
		return readInteger(Code(ctl), r)
	case ErrorCode:
		return Message{}, readError(r)
	default:
		return Message{}, fmt.Errorf("unknown control code: %s | %b", ctl, ctl)
	}
}

func readInteger(ctl Code, r *bufio.Reader) (Message, error) {
	v, err := r.ReadByte()
	if err != nil {
		return Message{}, errors.Wrap(err, "reading byte for uint8")
	}
	return Message{ctl, v}, nil
}

func readError(r *bufio.Reader) error {
	str, err := r.ReadString('\n')
	if err != nil && err != io.EOF {
		return errors.Wrap(err, "couldn't read error message")
	}
	return errors.New(strings.TrimSpace(str))
}
