// Package protocol provides functionality to interface with an arduino via
// serial, using the sharkio protocol
package protocol

import (
	"bufio"
	"github.com/tarm/serial"
	"io"
)

type Connection struct {
	rw     io.ReadWriteCloser
	writer *bufio.Writer
	reader *bufio.Reader
	config *serial.Config
}

func NewConnection(config *serial.Config) (*Connection, error) {
	c := &Connection{config: config}
	var err error
	c.rw, err = serial.OpenPort(c.config)
	c.reader = bufio.NewReader(c.rw)
	c.writer = bufio.NewWriter(c.rw)
	return c, err
}

func (c *Connection) Config() serial.Config {
	return *c.config
}

func (c *Connection) Close() {
	c.writer.Flush()
	c.rw.Close()
}

type Code rune

func (c Code) String() string {
	return string(c)
}

const (
	TempCode  Code = 't'
	PhCode    Code = 'p'
	ErrorCode Code = 'e'
)

type Message struct {
	Type  Code
	Value uint8
}
