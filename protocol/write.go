package protocol

import (
	"bufio"
)

func (c *Connection) WriteCommand(cmd Code) error {
	return writeCommand(c.writer, cmd)
}

func writeCommand(w *bufio.Writer, cmd Code) error {
	defer w.Flush()
	return w.WriteByte(byte(cmd))
}
