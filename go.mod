module gitlab.com/nebloc/sharkio

go 1.13

require (
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/sys v0.0.0-20200107162124-548cf772de50 // indirect
)
